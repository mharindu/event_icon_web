/*
 * File: app/controller/Main.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Sencha Touch 2.2.x library, under independent license.
 * License of Sencha Architect does not include license for Sencha Touch 2.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.controller.Main', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.MessageBox'],
    config: {
        refs: {
            loginView: 'userlogin',
            mainMenu: 'mainmenu',
            profile: 'profile',
            events: 'events',
            settings: 'settings',
            onAir: 'onair',
            aboutUs: 'aboutus',
            userList: 'userlist',
            userDetail: 'userdetail',
            userNavi: 'usernavi',
            messageNavi: 'messagesnavi',
            messageList: 'messageslist',
            messageDetail: 'messageDetail',
        },
        control: {
            mainMenu: {
                viewUserCommand: 'onViewUserCommand',
                viewEventsCommand: 'onViewEventsCommand',
                viewSettingsCommand: 'onViewSettingsCommand',
                viewOnAirCommand: 'onViewOnAirCommand',
                viewAboutUsCommand: 'onViewAboutUsCommand'

            },
            profile: {
                backCommand: 'onBackCommandProfile'
            },
            events: {
                backCommand: 'onBackCommandEvents'
            },
            settings: {
                backCommand: 'onBackCommandSettings'
            },
            onAir: {
                backCommand: 'onBackCommandOnAir'
            },
            aboutUs: {
                backCommand: 'onBackCommand'
            },
            userList: {
                disclose: 'onDisclose'
            },
            userDetail: {
                backCommand: 'onBackListCommand'
            },
            loginView: {
                signInCommand: 'onSignInCommand'
            },
            messageList: {
                disclose: 'onDiscloseMessage'
            },
            messageDetail: {
                backCommand: 'onBackMessageCommand'
            },
        }
    },
    sessionToken: null,
    hostUrl: 'http://localhost:8080/event_icon2/webresources/',
    //this.setSession(1),

    getSlideLeftTransition: function() {
        return {type: 'slide', direction: 'left'};
    },
    getSlideRightTransition: function() {
        return {type: 'slide', direction: 'right'};
    },
    onViewUserCommand: function() {


        var profile = Ext.create('MyApp.view.Profile', {
            //data: user.data,

        });

        var users = Ext.getStore('Users');
        console.log(users.getCount());
        var businesscards = Ext.getStore('BusinessCards');
        console.log(businesscards.getCount());
        var user = this.findData(users, 'user_id', this.sessionToken)
        console.log(user.get('user_name'));
        var businesscard = this.findData(businesscards, 'user_id', this.sessionToken);
        console.log(businesscard.get('email'));
        Ext.getCmp('firstName').setValue(user.get('first_name'));
        Ext.getCmp('lastName').setValue(user.get('last_name'));
        Ext.getCmp('otherName').setValue(user.get('other_names'));
        Ext.getCmp('organization').setValue(businesscard.get('organisation'));
        Ext.getCmp('designation').setValue(businesscard.get('designation'));
        Ext.getCmp('contactNo').setValue(businesscard.get('contact_number_1'));
        Ext.getCmp('email').setValue(businesscard.get('email'));






        Ext.Viewport.add(profile);

        Ext.Viewport.animateActiveItem(profile, this.getSlideLeftTransition());

    },
    findUser: function(userName) {
        var user = '';
        var users = Ext.getStore('Users');
        var size = users.getCount();
        for (var i = 0; i < size; i++) {
            user = users.getAt(i);
            if (user.get('user_name') == userName) {
                return user;
            }
        }
    },
    findBusinessCard: function(userId) {
        var user = '';
        var users = Ext.getStore('BusinessCards');
        var size = users.getCount();
        for (var i = 0; i < size; i++) {
            user = users.getAt(i);
            if (user.get('userId') == userId) {
                return user;
            }
        }
        return;
    },
    onBackCommand: function() {
        //console.log('tapped back');
        var mainMenu = this.getMainMenu();
        Ext.Viewport.animateActiveItem(mainMenu, this.getSlideRightTransition());
    },
    onBackCommandProfile: function() {
        //console.log('tapped back');
        var mainMenu = this.getMainMenu();
        //var profile = Ext.getCmp("profile");
        //profile.destroy();


        Ext.Viewport.animateActiveItem(mainMenu, this.getSlideRightTransition());
        Ext.Viewport.remove('MyApp.view.Profile', true);


    },
    onBackCommandEvents: function() {
        //console.log('tapped back');
        var mainMenu = this.getMainMenu();

        Ext.Viewport.animateActiveItem(mainMenu, this.getSlideRightTransition());
        Ext.Viewport.remove('MyApp.view.Events', true);


    },
    onBackCommandSettings: function() {
        //console.log('tapped back');
        var mainMenu = this.getMainMenu();

        Ext.Viewport.animateActiveItem(mainMenu, this.getSlideRightTransition());
        Ext.Viewport.remove('MyApp.view.Settings', true);


   },
    onBackCommandOnAir: function() {
        //console.log('tapped back');
        var mainMenu = this.getMainMenu();

        Ext.Viewport.animateActiveItem(mainMenu, this.getSlideRightTransition());
        Ext.Viewport.remove('MyApp.view.OnAir', true);


    },
    onViewEventsCommand: function() {
        //console.log('tapped events');
        var eventusers = Ext.getStore('EventUsers');
        console.log(eventusers.getCount());

        var userevents = this.findMultipleData(eventusers, 'user_id', this.sessionToken);
        console.log(userevents.getCount());

        var aevents = Ext.getStore('AEvents');
        console.log(eventusers.getCount());

        var parevents = this.findManytoManyData(userevents, aevents, 'event_id', this.sessionToken);
        console.log(parevents.getCount());

        var events = Ext.create('MyApp.view.Events', {
            data: parevents.data,
        });

        Ext.Viewport.add(events);
        Ext.Viewport.animateActiveItem(events, this.getSlideLeftTransition());
    },
    onViewSettingsCommand: function() {
        var settings = Ext.create('MyApp.view.Settings', {
        });

        Ext.Viewport.add(settings);
        Ext.Viewport.animateActiveItem(settings, this.getSlideLeftTransition());
    },
    onViewOnAirCommand: function() {

        var aevents = Ext.getStore('AEvents');
        console.log(aevents.getCount());





        var onair = Ext.create('MyApp.view.OnAir', {
            //data: aevents.getAt(0),
        });


        Ext.Viewport.add(onair);
        Ext.Viewport.animateActiveItem(onair, this.getSlideLeftTransition());

    },
    onViewAboutUsCommand: function() {
        //console.log('tapped aboutUs');
        var aboutUs = this.getAboutUs();
        Ext.Viewport.animateActiveItem(aboutUs, this.getSlideLeftTransition());
    },
    onDisclose: function(list, record) {
        //console.log('disclosed');
        this.getUserNavi().push(
                {
                    xtype: 'userdetail',
                    data: record.data,
                    //title: record.fullName(),
                }
        );
        //Ext.Viewport.animateActiveItem(userDetail, this.getSlideLeftTransition());
    },
    onDiscloseMessage: function(list, record) {
        //console.log('disclosed');
        this.getMessageNavi().push(
                {
                    xtype: 'messagedetail',
                    data: record.data,
                    //title: record.fullName(),
                }
        );
        //Ext.Viewport.animateActiveItem(userDetail, this.getSlideLeftTransition());
    },
    onBackListCommand: function(record) {
        //console.log('tapped back');
        var list = this.getUserList();
        Ext.Viewport.animateActiveItem(list, this.getSlideRightTransition());
    },
    onBackMessageCommand: function(record) {
        //console.log('tapped back');
        var list = this.getMessageList();
        Ext.Viewport.animateActiveItem(list, this.getSlideRightTransition());
    },
    getSlideLeftTransition: function() {
        return {type: 'slide', direction: 'left'};
    },
            getSlideRightTransition: function() {
        return {type: 'slide', direction: 'right'};
    },
    onSignInCommand: function(view, username, password) {

        var me = this,
                loginView = me.getLoginView();

        if (username.length === 0 || password.length === 0) {
            console.log("Feilds are empty");
            loginView.setMasked(false);
            Ext.Msg.alert('Alert', 'Fields are empty', Ext.emptyFn);
            return;
        }
        var users = Ext.getStore('Users');
        console.log(users.getCount());
        console.log(username);
        //if(username = ) {
        this.findUser(username);
        
//        if(!user){
//            return;
//        }
        //}


        me.signInSuccess();     ///Just simulating success.

    },
    signInSuccess: function(user) {
        console.log('Signed in.');
        var loginView = this.getLoginView();
        var mainMenuView = this.getMainMenu();

        //loading mask stopped
        loginView.setMasked(false);
        
        this.setSession(user);
        console.log('current user:' + this.sessionToken, this.hostUrl);
        Ext.Viewport.animateActiveItem(mainMenuView, this.getSlideLeftTransition());
    },
    setSession: function(token) {
        this.sessionToken = token;
    },
    distroySession: function() {
        this.sessionToken = null;
    },
    loadData: function(url) {
        var temp;
        Ext.Ajax.request({
            url: url,
            method: 'GET',
            success: function(response) {
                var text = response.responseText;
                data = Ext.decode(text, true);

                temp = data;
                console.log('loaded items ' + temp.length);
            }
        });
        return temp;
    },
    loadDatabase: function() {

//        
//        //loading user data
//        console.log('user');
//        var url = this.hostUrl.concat("user");     
//        this.userEntity = this.loadData(this.userEntity,url);
//        console.log('mokakda yakooo'+this.userEntity.length);
//        
        //loading businesscard
//        console.log('businesscard');
//        url = this.hostUrl.concat("businesscard");
//        this.loadData(this.businessCardEntity,url);
//        
//        //loading businesscard
//        console.log('businesscard');
//        url = this.hostUrl.concat("businesscard");
//        this.loadData(this.businessCardEntity,url);
//        
//        //loading businesscard
//        console.log('adminmessages');
//        url = this.hostUrl.concat("adminmessage");
//        this.loadData(this.adminMessageEntity,url);
//        
//         //loading agendaElement
//        console.log('agendaElement');
//        url = this.hostUrl.concat("agendaelement");
//        this.loadData(this.agendaElementEntity,url);
//
//         //loading event
//        console.log('event');
//        url = this.hostUrl.concat("event");
//        this.loadData(this.eventEntity,url);
//        
//        //loading eventuser
//        console.log('event_user');
//        url = this.hostUrl.concat("eventuser");
//        this.loadData(this.eventUserEntity,url);
//        
//        //loading expirience
//        console.log('experience');
//        url = this.hostUrl.concat("experience");
//        this.loadData(this.experienceEntity,url);
//
//        //loading honors
//        console.log('honors');
//        url = this.hostUrl.concat("honors");
//        this.loadData(this.honorsEntity,url);
//        
////        //loading meterial
////        console.log('meterial');
////        url = this.hostUrl.concat("material");
////        this.loadData(this.materialEntity,url);
//
//        //loading organization
//        console.log('organization');
//        url = this.hostUrl.concat("organisation");
//        this.loadData(this.organisationEntity,url);
//        
//        //loading organizer
//        console.log('organizer');
//        url = this.hostUrl.concat("organizer");
//        this.loadData(this.organizerEntity,url);
//
//        //loading pastevents
//        console.log('pastevents');
//        url = this.hostUrl.concat("organisation");
//        this.loadData(this.organisationEntity,url);
//
//        //loading projects
//        console.log('projects');
//        url = this.hostUrl.concat("projects");
//        this.loadData(this.projectsEntity,url);
//  
//        //loading projects
//        console.log('projects');
//        url = this.hostUrl.concat("projects");
//        this.loadData(this.projectsEntity,url);
//        
//        //loading public profile
//        console.log('public_profile');
//        url = this.hostUrl.concat("publicprofile");
//        this.loadData(this.publicProfileEntity,url);
//        
//        //loading recentactivity
//        console.log('recentactivity');
//        url = this.hostUrl.concat("recentactivity");
//        this.loadData(this.recentActivityEntity,url);
//        
//        //loading settings
//        console.log('settings');
//        url = this.hostUrl.concat("settings");
//        this.loadData(this.settings,url);
//        
//        //loading userorganizer
//        console.log('userorganizer');
//        url = this.hostUrl.concat("userorganizer");
//        this.loadData(this.userOrganizerEntity,url);
//        
//        //loading userorganizer
//        console.log('userorganizer');
//        url = this.hostUrl.concat("userorganizer");
//        this.loadData(this.userOrganizerEntity,url);
//    
//        //loading useruser
//        console.log('useruser');
//        url = this.hostUrl.concat("useruser");
//        this.loadData(this.userUserEntity,url);
    },
    findData: function(records, search_field, field) {

        for (var i = 0; i < records.getCount(); i++) {
            if (records.getAt(i).get(search_field) == field) {
                return records.getAt(i);
            }

        }
    },
    findMultipleData: function(records, search_field, field) {
        var flag = true;
        for (var i = 0; i < records.getCount(); i++) {
            if (records.getAt(i).get(search_field) != field) {
                records.removeAt(i);
                i = 0;
            }
        }
        return records;
    },
    findManytoManyData: function(record1, record2, search_field, field1) {


        for (var i = 0; i < record1.getCount(); i++) {
            var match = false;
            for (var j = 0; j < record2.getCount(); j++) {
                if (record2.getAt(j).get(search_field) == record1.getAt(i).get(search_field)) {
                    console.log(record2.getAt(j).get(search_field) + "==" + record1.getAt(i).get(search_field));
                    match = true;
                    break;
                }
            }
            if (match == false) {
                record1.removeAt(i);
                i = i - 1;
            }
        }
        return record1;
    },
});