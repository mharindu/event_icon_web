Ext.define('MyApp.controller.Login', {
    extend: 'Ext.app.Controller',
    config: {
		refs: {
            loginView: 'userlogin',
            mainMenuView: 'mainmenu'
        },
        control: {
            loginView: {
                signInCommand: 'onSignInCommand'
            }
        }
    },
	getSlideLeftTransition: function () {
        return { type: 'slide', direction: 'left' };
    },

    getSlideRightTransition: function () {
        return { type: 'slide', direction: 'right' };
    },

    onSignInCommand: function (view, username, password) {

        //console.log('Username: ' + username + '\n' + 'Password: ' + password);

        var me = this,
        loginView = me.getLoginView();

        if (username.length === 0 || password.length === 0) {
          //  console.log("Feilds are empty");
			return;
        }

        
         me.signInSuccess();     ///Just simulating success.
              
    },

    signInSuccess: function () {
        console.log('Signed in.');
        var loginView = this.getLoginView();
        var mainMenuView = this.getMainMenuView();
        //loginView.setMasked(false);

		loginView.setMasked(false);
        Ext.Viewport.animateActiveItem(mainMenuView, this.getSlideLeftTransition());
    },
});