Ext.define('MyApp.store.AEvents', {
    extend: 'Ext.data.Store',
    alias: "widget.aevents",
    config: {
        model: 'MyApp.model.Event',
        data: [
            {event_id: 2, org_id: '20', event_name: "Award Ceremony", description: "Slim Nelson People Award ceremony", type: 2, start_time: "18:00:00", end_time: "21:00:00", date: "2013-06-17", address_L1: "No:23", address_L2: "2nd Cross Street", address_L3: "Melborn", country: "Australia", ticket_des: "Ticket Price... Rs. 500/=", profile_pic: "null"},
            {event_id: 3, org_id: '10', event_name: "Imagine Cup", description: "Microsoft Imagine Cup Competetion Local Final", type: 1, start_time: "14:00:00", end_time: "17:00:00", date: "2013-07-01", address_L1: "University of Colombo", address_L2: "Reid Avenue", address_L3: "Colombo 07", country: "Sri Lanka", ticket_des: "Free...", profile_pic: "null"},
            {event_id: 4, org_id: '10', event_name: "Xtreame", description: "IEEE Xtreame programming competetion 6.0.", type: 1, start_time: "00:00:00", end_time: "23:59:59", date: "2013-07-12", address_L1: "University of Colombo", address_L2: "Reid Avenue", address_L3: "Colombo 07", country: "Sri Lanka", ticket_des: "Free...", profile_pic: "null"},
            {event_id: 5, org_id: '30', event_name: "BarCamp", description: "Google BarCamp", type: 3, start_time: "09:00:00", end_time: "17:00:00", date: "2013-08-23", address_L1: "University of Colombo", address_L2: "Reid Avenue", address_L3: "Colombo 07", country: "Sri Lanka", ticket_des: "Free...", profile_pic: "null"},
        ]
    }
});

