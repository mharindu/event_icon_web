Ext.define('MyApp.store.AgendaElements', {
    extend: 'Ext.data.Store',
	alias: "widget.agendaelements",
	
	config: {
		model: 'MyApp.model.AgendaElements',
				
		data: [
		{ event_id:1, element_id:1, title:"Smart Phones", presenter:"Mr. Karthik Fernando", description:"Lecture about Smart Phones", start_time:"09:00:00", end_time:"10:00:00"},
		{ event_id:1, element_id:2, title:"Competetion", presenter:"Mrs. Subhani Harshani", description:"Competetion between participants", start_time:"10:00:00", end_time:"15:00:00"},
		{ event_id:1, element_id:3, title:"Prize Giving", presenter:"Mrs. Dinithi Prasanga", description:"Prize Giving to winners", start_time:"15:00:00", end_time:"15:45:00"},
		{ event_id:1, element_id:4, title:"Vote of Thank", presenter:"Mr. D. A. U. Bandara", description:"Vote of Thanks...", start_time:"15:45:00", end_time:"16:00:00"},
		{ event_id:2, element_id:1, title:"Welcome Speech", presenter:"Mr. A. G. Perera", description:"Address the Participants...", start_time:"18:00:00", end_time:"18:30:00"},
		{ event_id:2, element_id:2, title:"Awards", presenter:"Mrs. Lilani Gunathilaka", description:"Awards for winners", start_time:"18:30:00", end_time:"20:30:00"},
		{ event_id:2, element_id:3, title:"Vote of Thank", presenter:"Mr. Lakmal Weerawarna", description:"Vpte of thanks...", start_time:"20:30:00", end_time:"21:00:00"},
		{ event_id:3, element_id:1, title:"Competetion", presenter:"Miss. W.A. D. Ramanayake", description:"Competetion between teams", start_time:"14:00:00", end_time:"16:00:00"},
		{ event_id:3, element_id:2, title:"Prize Giving", presenter:"Dr. Walter Fernandz", description:"Prize Giving to winners", start_time:"16:00:00", end_time:"18:45:00"},
		{ event_id:3, element_id:3, title:"Refreshment", presenter:"No Presenter", description:"There is small refreshments in cafetaria", start_time:"18:45:00", end_time:"19:00:00"},
		{ event_id:4, element_id:1, title:"Competetion", presenter:"No Presenter", description:"Competetion between participant teams..", start_time:"00:00:00", end_time:"07:00:00"},
		{ event_id:4, element_id:2, title:"Breakfast", presenter:"No Presenter", description:"In Cafetaria.", start_time:"07:00:00", end_time:"07:30:00"},
		{ event_id:4, element_id:3, title:"Competetion", presenter:"No Presenter", description:"Competetion between participant teams..", start_time:"07:30:00", end_time:"12:30:00"},
		{ event_id:4, element_id:4, title:"Lunch", presenter:"No Presenter", description:"In Cafetaria.", start_time:"12:30:00", end_time:"13:30:00"},
		{ event_id:4, element_id:5, title:"Competetion", presenter:"No Presenter", description:"Competetion between participant teams..", start_time:"13:30:00", end_time:"20:00:00"},
		{ event_id:4, element_id:6, title:"Dinner", presenter:"No Presenter", description:"In Cafetaria.", start_time:"20:00:00", end_time:"20:45:00"},
		{ event_id:4, element_id:7, title:"Competetion", presenter:"No Presenter", description:"Competetion between participant teams..", start_time:"20:45:00", end_time:"23:15:00"},
		{ event_id:4, element_id:8, title:"Prize Giving", presenter:"Mrs. Dinithi Prasanga", description:"Prize Giving to winners", start_time:"23:15:00", end_time:"23:59:59"},
		{ event_id:5, element_id:1, title:"Smart Phones", presenter:"Dr. Walter Fernandz", description:"Lecture about Smart Phones", start_time:"09:00:00", end_time:"11:00:00"},
		{ event_id:5, element_id:2, title:"Google", presenter:"Dr. Ann Rozalle", description:"Head of Google - Sri Lanka...", start_time:"11:00:00", end_time:"12:30:00"},
		{ event_id:5, element_id:3, title:"Lunch", presenter:"No Presenter", description:"In Cafetaria.", start_time:"12:30:00", end_time:"13:30:00"},
		{ event_id:5, element_id:4, title:"Google Drive", presenter:"Mr. D. S. F. Fayaz", description:"Introduction about google drive...", start_time:"13:30:00", end_time:"16:30:00"},
		{ event_id:5, element_id:5, title:"Vote of Thakns", presenter:"Mr. Lakmal Weerawarna", description:"Vote of Thanks...", start_time:"16:30:00", end_time:"17:00:00"},
		]
    },

});

