Ext.define('MyApp.store.BusinessCards', {
    extend: 'Ext.data.Store',
	alias: "widget.businesscards",
	
	config: {
		model: 'MyApp.model.BusinessCard',
				
		data: [
		{ user_id:"1", designation:"Software Engineer", organisation:"99x Technologies", email:"mharindu.ucsc@gmail.com", contact_number_1:"0773332016", contact_number_2:"0712317867", profile_summary:"Award winning editor and technical writer with five years of experience.", website:"mharindu@blogspot.com", address_L1:"No:10, Perera Mawatha", address_L2:"Alubomulla", address_L3:"Panadura", country:"Sri Lanka"},
		{ user_id:"2", designation:"System Admin", organisation:"Abans Co PVT LTD.", email:"amscata@gmail.com", contact_number_1:"0772625663", contact_number_2:"0754313245", profile_summary:"Successfully ipmlepent current web design technology to develop and maintain sites for start-up IT companies.", website:"amscata@blogspot.com", address_L1:"Muthuhara, Manel Mawatha", address_L2:"Mahawila", address_L3:"Kalutara", country:"Sri Lanka"},
		{ user_id:"3", designation:"Web Developer", organisation:"BSharp Technologies Pvt LTD.", email:"randarasa@ieee.org", contact_number_1:"0782726828", contact_number_2:"0756021078", profile_summary:"Certified workforce development professional.", website:"randarasa@blogspot.com", address_L1:"08, S. Mahinda Mawatha", address_L2:"Alubomulla", address_L3:"Panadura", country:"Sri Lanka"},
		{ user_id:"4", designation:"Accountant", organisation:"Kalutota Finance Co.LTD.", email:"edishanthy@gmail.com", contact_number_1:"0782726827", contact_number_2:"0756021073", profile_summary:"Experienced marketing executive.", website:"eid.x86@blogspot.com", address_L1:"No:99/7B", address_L2:"Seylam Bridge", address_L3:"Nawalapitiya", country:"Sri Lanka"},
		{ user_id:"6", designation:"Office Executive", organisation:"Dialog Axita PLC.", email:"edilanthy@yahoo.com", contact_number_1:"0775923452", contact_number_2:"0712301234", profile_summary:"Production assistant for nationally broadcast television series.", website:"abcdix@blogspot.com", address_L1:"No:88, Hospital Road", address_L2:"Martin Lan", address_L3:"Jaffna", country:"Sri Lanka"}
		]
    },

});