Ext.define('MyApp.store.PublicProfiles', {
    extend: 'Ext.data.Store',
	alias: "widget.publicprofiles",
	
	config: {
		model: 'MyApp.model.PublicProfile',
				
		data: [
		{ user_id:1, profile_id:1, xml_path:"https://www.facebook.com/milanharindu"},
		{ user_id:1, profile_id:2, xml_path:"https://twitter.com/milan"},
		{ user_id:2, profile_id:1, xml_path:"https://www.facebook.com/amscata"},
		{ user_id:2, profile_id:2, xml_path:"https://www.linkedin.com/uas/login"},
		{ user_id:2, profile_id:3, xml_path:"https://twitter.com/amscata"},
		{ user_id:3, profile_id:1, xml_path:"https://www.facebook.com/rbrasanga"},
		{ user_id:3, profile_id:2, xml_path:"https://twitter.com/rbrasanga"},
		{ user_id:4, profile_id:1, xml_path:"https://www.facebook.com/iresha.dishanthy"},
		{ user_id:4, profile_id:2, xml_path:"http://friendfinder.com/p/dilanthy"},
		{ user_id:6, profile_id:1, xml_path:"https://www.facebook.com/joyline.joy"},
		]
    },
	

});