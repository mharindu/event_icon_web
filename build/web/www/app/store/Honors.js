Ext.define('MyApp.store.Honors', {
    extend: 'Ext.data.Store',
	alias: "widget.honors",
	
	config: {
		model: 'MyApp.model.Honor',
				
		data: [
		{ user_id:1, honor_id:1, title:"BSc. in CS", description:"University of Colombo", year:"2012"},
		{ user_id:2, honor_id:1, title:"BSc. in CS", description:"Universiy of Peradeniya", year:"2010"},
		{ user_id:2, honor_id:2, title:"DR.", description:"University of Walse", year:"2012"},
		{ user_id:1, honor_id:1, title:"BCSc.", description:"University of Colombo", year:"2012"},
		{ user_id:1, honor_id:1, title:"ACIMA", description:"Charted Institute of Management Accountancy.", year:"2011"},
		{ user_id:1, honor_id:1, title:"TSC|SSU", description:"City $ Guilds", year:"2010"},
		{ user_id:6, honor_id:1, title:"BSc.(Mathematics)", description:"University of Sri Jayawardana", year:"2009"},
		{ user_id:6, honor_id:2, title:"MSc.(Applied Science)", description:"Universiy of Peradeniya", year:"2012"},
		]
    },
	

});