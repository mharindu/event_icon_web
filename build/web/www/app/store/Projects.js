Ext.define('MyApp.store.Projects', {
    extend: 'Ext.data.Store',
	alias: "widget.projects",
	
	config: {
		model: 'MyApp.model.Project',
				
		data: [
		{ user_id:1, project_id:1, project_name:"Sumangala Web", description:"A web site for Sri Sumangala Girl's College", year:"2011"},
		{ user_id:1, project_id:2, project_name:"EVENTiCON", description:"System for event organizars and participants.", year:"2013"},
		{ user_id:2, project_id:1, project_name:"EVENTiCON", description:"System for event organizars and participants.", year:"2013"},
		{ user_id:3, project_id:1, project_name:"EVENTiCON", description:"System for event organizars and participants.", year:"2013"},
		{ user_id:4, project_id:1, project_name:"Currency System", description:"A cach flow system", year:"2013"},
		{ user_id:5, project_id:1, project_name:"Vacum Meter", description:"Vacum meter for vehicle tyres.", year:"2011"},
		{ user_id:5, project_id:2, project_name:"EzCash", description:"Cash system for Dialog", year:"2012"},
		]
    },
	

});