Ext.define('MyApp.store.Organisations', {
    extend: 'Ext.data.Store',
	alias: "widget.organisations",
	
	config: {
		model: 'MyApp.model.Organisation',
				
		data: [
		{ user_id:1, organization_id:1, title:"Software Engineer", description:"At 99x Technologies", start_date:"2012-05-23", end_date:"2013-06-17"},
		{ user_id:2, organization_id:1, title:"System Admin", description:"At  Abans Co. PVT. LTD.", start_date:"2012-01-12", end_date:"2013-06-17"},
		{ user_id:3, organization_id:1, title:"Web Developer", description:"At BSharp Technologies. ", start_date:"2011-06-14", end_date:"2013-06-17"},
		{ user_id:4, organization_id:1, title:"Assistant Accountant", description:"At Bartleet Finance Co. LTD.", start_date:"2010-01-16", end_date:"2012-06-30"},
		{ user_id:4, organization_id:2, title:"Accountant", description:"At Kalutota Finance", start_date:"2012-07-01", end_date:"2013-06-17"},
		{ user_id:5, organization_id:1, title:"Technical Officer", description:"At Dialog Axita PLC.", start_date:"2009-01-01", end_date:"2011-06-20"},
		{ user_id:5, organization_id:2, title:"Office In Charge", description:"At Dialog Axita PLC.", start_date:"2011-06-21", end_date:"2013-06-17"},
		{ user_id:6, organization_id:1, title:"Office Executive", description:"At Dialog Axita PLC.", start_date:"2012-03-20", end_date:"2013-06-17"},
		]
    },
	

});