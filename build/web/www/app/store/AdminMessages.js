Ext.define('MyApp.store.AdminMessages', {
    extend: 'Ext.data.Store',
	alias: "widget.adminmessages",
	
	config: {
		model: 'MyApp.model.AdminMessage',
				
		data: [
		{ message_id:"1", user_id:"3", type:"1", subjecct:"Hello", message:"This system is very good", state:"0", time_stamp:"2013-06-16 19:12:28"},
		{ message_id:"2", user_id:"4", type:"1", subjecct:"System", message:"You guys did a great job", state:"0", time_stamp:"2013-06-16 19:12:28"},
		{ message_id:"3", user_id:"3", type:"1", subjecct:"Again", message:"I' proud of you guys.", state:"0", time_stamp:"2013-06-16 10:53:00"},
		{ message_id:"4", user_id:"3", type:"0", subjecct:"Thanks", message:"Thank You...", state:"1", time_stamp:"2013-06-16 19:12:28"},
		{ message_id:"5", user_id:"4", type:"0", subjecct:"Thanks", message:"Thank You...", state:"1", time_stamp:"2013-06-16 19:12:28"},
		]
    },
	

});