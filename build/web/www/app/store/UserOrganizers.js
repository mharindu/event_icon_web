Ext.define('MyApp.store.UserOrganizers', {
    extend: 'Ext.data.Store',
	alias: "widget.userorganizers",
	
	config: {
		model: 'MyApp.model.UserOrganizer',
				
		data: [
		{ message_id:1, event_id:1, sender_id:3, receiver_id:10, subject:"Water Bottle", message:"Can you send me a water bottle. Please.", state:1, time_stamp:"2013-05-30 11:09:12"},
		{ message_id:2, event_id:1, sender_id:4, receiver_id:10, subject:"MP3", message:"Can you send me MP3 again", state:1, time_stamp:"2013-05-30 12:09:12"},
		{ message_id:3, event_id:1, sender_id:10, receiver_id:4, subject:"MP3", message:"Ok. I'll send you immediately.", state:1, time_stamp:"2013-05-30 12:10:22"},
		{ message_id:4, event_id:1, sender_id:3, receiver_id:10, subject:"MP4", message:"Can you send me MP4 again", state:0, time_stamp:"2013-05-30 13:09:12"},
		]
    },
	

});