Ext.define('MyApp.store.PastEvents', {
    extend: 'Ext.data.Store',
	alias: "widget.pastevents",
	
	config: {
		model: 'MyApp.model.PastEvent',
				
		data: [
		{org_id:10, event_id:1, event_name:"Google IO", description:"Android Development Competetion", year:"2013"},
		]
    },
	

});