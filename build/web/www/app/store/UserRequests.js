Ext.define('MyApp.store.UserRequests', {
    extend: 'Ext.data.Store',
	alias: "widget.userrequests",
	
	config: {
		model: 'MyApp.model.UserRequest',
				
		data: [
		{message_id:1, user_id:7, event_id:3},
		{message_id:2, user_id:6, event_id:4},
		{message_id:3, user_id:7, event_id:3},
		]
    },
	

});