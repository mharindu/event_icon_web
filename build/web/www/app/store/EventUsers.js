Ext.define('MyApp.store.EventUsers', {
    extend: 'Ext.data.Store',
    alias: "widget.eventusers",
    config: {
        model: 'MyApp.model.EventUser',
        data: [
            {event_id: 1, user_id: 1, verify: 0},
            {event_id: 1, user_id: 3, verify: 1},
            {event_id: 1, user_id: 4, verify: 1},
            {event_id: 2, user_id: 1, verify: 1},
            {event_id: 2, user_id: 2, verify: 0},
            {event_id: 3, user_id: 5, verify: 0},
            {event_id: 3, user_id: 6, verify: 0},
            {event_id: 4, user_id: 1, verify: 0},
            {event_id: 4, user_id: 2, verify: 0},
            {event_id: 4, user_id: 3, verify: 0},
            {event_id: 4, user_id: 4, verify: 0},
            {event_id: 5, user_id: 1, verify: 0},
            {event_id: 5, user_id: 5, verify: 0},
            {event_id: 5, user_id: 6, verify: 0},
        ]
    },
});