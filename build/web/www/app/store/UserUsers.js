Ext.define('MyApp.store.UserUsers', {
    extend: 'Ext.data.Store',
	alias: "widget.userusers",
	
	config: {
		model: 'MyApp.model.UserUser',
				
		data: [
		{ message_id:1, event_id:1, sender_id:"randa", receiver_id:"easterline", subject:"Hello", message:"Hello, How are you?", state:"1", time_stamp:"2013-06-13 09:35:58"},
		{ message_id:2, event_id:1, sender_id:"easterline", receiver_id:"randa", subject:"Hi", message:"I'd Like to meet you.", state:"1", time_stamp:"2013-06-13 09:45:58"},
		{ message_id:3, event_id:1, sender_id:"randa", receiver_id:"easterline", subject:"Reply", message:"Ok. Meet me after the event.", state:"1", time_stamp:"2013-06-13 10:53:00"},
		{ message_id:4, event_id:1, sender_id:"easterline", receiver_id:"randa", subject:"Reply", message:"Thakn you very much... :)", state:"1", time_stamp:"2013-06-13 11:23:01"},
		{ message_id:5, event_id:1, sender_id:"randa", receiver_id:"easterline", subject:"Reply", message:"You are wellcome...", state:"1", time_stamp:"2013-06-13 11:23:56"},
		]
    },
	

});