Ext.define('MyApp.store.Experiences', {
    extend: 'Ext.data.Store',
	alias: "widget.experiences",
	
	config: {
		model: 'MyApp.model.Experience',
				
		data: [
		{ user_id:1, experience_id:1, title:"Software Engineer", description:"Increased annual departmental turnover by 180% whi...", year:"2012"},
		{ user_id:2, experience_id:1, title:"System Admin", description:"Entered three new markets in Spain, Italy and Germ...", year:"2012"},
		{ user_id:3, experience_id:1, title:"Web Developer", description:"Organised the 2006 Made Up Gaming Poker open which...", year:"2012"},
		{ user_id:4, experience_id:1, title:"Assistant Accountant", description:"Bartleet Finance Co. LTD.", year:"2010"},
		{ user_id:4, experience_id:2, title:"Accountant", description:"Responsible for a marketing budget of £2m.", year:"2012"},
		{ user_id:6, experience_id:1, title:"Office Executive", description:"Dialog Axita PLC.", year:"2012"},
		]
    },
	

});