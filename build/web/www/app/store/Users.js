Ext.define('MyApp.store.Users', {
    extend: 'Ext.data.Store',
	alias: "widget.users",
	
	config: {
		model: 'MyApp.model.User',
		sorter: 'first_name',
		grouper: function(record) {
		  return record.get('first_name')[0];
		},		
		data: [
		  { user_id:1, first_name:"milan", last_name:"perera", other_names:"harindu", user_name:"admin", password:"d6c7a310f019f194fbe7ac7ddfbdb69864695928", profile_pic:"null"},
		  { user_id:2, first_name:"amith", last_name:"lakmal", other_names:"chinthaka", user_name:"amscata", password:"a2b00fff6d4a613b9b10741fe65d72d470625a3e", profile_pic:"null"},
		  { user_id:3, first_name:"randa", last_name:"bandara", other_names:"rasanga", user_name:"rbrasanga", password:"4904aa0386d8a31bbda865c90b6476d6f558f844", profile_pic:"null"},
		  { user_id:4, first_name:"easterline", last_name:"david", other_names:"dishanthy", user_name:"edishanthy", password:"028a9a50c5ce122d190be5e5b78a4c895db3515e", profile_pic:"null"},
		  { user_id:5, first_name:"amila", last_name:"bandara", other_names:"darshana", user_name:"adubandara", password:"63bbf927f5e858a723f6cb06df659cdb9dfbb987", profile_pic:"null"},
		  { user_id:6, first_name:"evangaline", last_name:"david", other_names:"dilanthy", user_name:"edilanthy", password:"6cc1fd5715659a95ba17ae83e0b5105f7f137139", profile_pic:"null"},
		  { user_id:7, first_name:"rishan", last_name:"bandara", other_names:"randitha", user_name:"risranditha", password:"821bd23c87eeb5bfaa903083fb039ea929c1a455", profile_pic:"null"},
		  { user_id:10, first_name:"rahal", last_name:"weerasooriya", other_names:"madduma", user_name:"mobitel", password:"611b494e2d92e4a3a74485916c8225d96deb5321", profile_pic:"null"},
		  { user_id:20, first_name:"kusal", last_name:"fernando", other_names:"janith", user_name:"derana", password:"afeebb5eb08d4fa7a01fb94dcff6eb46771fc265", profile_pic:"null"},
		  { user_id:30, first_name:"kumara", last_name:"perera", other_names:"charith", user_name:"ucsc", password:"0a1395ce7d53296ccd9bf2068698334ec6381dbb", profile_pic:"null"},
		]
    },
	

});