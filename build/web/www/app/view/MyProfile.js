Ext.define('MyApp.view.MyProfile', {
    extend: 'Ext.form.Panel',
    alias: "widget.myprofile",
    xtype: 'myprofile',
    config: {
     items: [
            {
                xtype: 'fieldset',
                instructions: 'Current profile details',
                items: [
                    {
                        xtype: 'image',
                        height: 201,
                        margin: '0px 0px 10px 0px',
                        width: '',
                        itemId: 'userImage',
                        name: 'profilePic',
                        src: 'resources/images/user.png'
                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'First Name',
                        labelWidth: '40%',
                        id: 'firstName',
                        name: 'firstName',
                        disabled: 'false'
                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'Last Name',
                        labelWidth: '40%',
                        id: 'lastName',
                        name: 'lastName',
                        disabled: 'false'

                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'Other Name',
                        labelWidth: '40%',
                        id: 'otherName',
                        name: 'otherName',
                        disabled: 'false'

                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'Organization',
                        labelWidth: '40%',
                        itemId: 'organization',
                        id: 'organization',
                        name: 'organization',
                        disabled: 'false'
                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'Designation',
                        labelWidth: '40%',
                        itemId: 'designation',
                        id: 'designation',
                        name: 'designation',
                        disabled: 'false'
                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'Contact No',
                        labelWidth: '40%',
                        itemId: 'contactNo',
                        id: 'contactNo',
                        name: 'contactNumber1',
                        disabled: 'false'
                    },
                    {
                        xtype: 'textfield',
                        margin: '0px 0px 2px 0px',
                        label: 'Email',
                        labelWidth: '40%',
                        itemId: 'email',
                        id: 'email',
                        name: 'email',
                        disabled: 'false'
                    }

                ],
            }

        ],
    },
});