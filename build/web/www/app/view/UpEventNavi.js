Ext.define('MyApp.view.UpEventNavi', {
    extend: 'Ext.navigation.View',
	alias: "widget.upeventnavi",
	xtype: 'upeventnavi',
	requires: [
	  'MyApp.view.UpEventList',
	  //'MyApp.view.UserDetail'
	],
	
	config: {
		items: [
		  {
			xtype: 'upeventlist'
		  }
		]
    },
	
	
});