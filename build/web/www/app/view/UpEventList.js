Ext.define('MyApp.view.UpEventList', {
    extend: 'Ext.List',
	alias: "widget.upeventlist",
	xtype: 'upeventlist',
	
	config: {
		//title: 'Participant List',
		//grouped: true,
		itemTpl: "<div style='float:left;width:60px'><img src='resources/images/user.png' width='30px' height='30px'/></div><div style='margin-left:62px'>{event_name} ({date})</div>",
		store: 'AEvents',
		onItemDisclosure: true,
    },
	
	
});


