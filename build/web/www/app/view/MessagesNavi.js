Ext.define('MyApp.view.MessagesNavi', {
    extend: 'Ext.navigation.View',
	alias: "widget.messagesnavi",
	xtype: 'messagesnavi',
	requires: [
	  'MyApp.view.MessagesList',
	  'MyApp.view.MessageDetail'
	],
	
	config: {
		items: [
		  {
			xtype: 'messageslist'
		  }
		]
    },
	
	
});