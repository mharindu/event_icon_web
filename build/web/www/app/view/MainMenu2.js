Ext.define('MyApp.view.MainMenu2', {
    extend: 'Ext.Panel',
	alias: "widget.mainmenu2",
	requires: ['Ext.TitleBar'],
	
    config: {
		items: [
			{
			  xtype: 'titlebar',
			  docked: 'top',
			  title: 'Main Menu'
			},
		   
		   {
                xtype: 'button',
                
                height: 60,
                itemId: 'ProfileButton',
                margin: 10,
                padding: 10,
                styleHtmlContent: true,
                ui: 'plain',
                icon: 'true',
                iconAlign: 'top',
                iconCls: 'user',
                text: 'Profile'
            },
	  
		],
		
		listeners: [
		  {
            delegate: '#ProfileButton',
            event: 'tap',
            fn: 'onProfileButtonTap'
		  },
		]   
	},
	
	onProfileButtonTap: function() {
	  var me = this;
	  me.fireEvent('viewUserCommand', me);
	},

});