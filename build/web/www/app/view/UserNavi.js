Ext.define('MyApp.view.UserNavi', {
    extend: 'Ext.navigation.View',
	alias: "widget.usernavi",
	xtype: 'usernavi',
	requires: [
	  'MyApp.view.UserList',
	  'MyApp.view.UserDetail'
	],
	
	config: {
		items: [
		  {
			xtype: 'userlist'
		  }
		]
    },
	
	
});