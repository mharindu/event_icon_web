MyApp.views.Viewport = Ext.extend(Ext.Panel, {
	fullscreen: true,
	layout: 'card',

	initComponent: function(){
		Ext.apply(this, {
			items:[
				{xtype: 'MyApp.views.UserLogin', id: 'userLogin'},
				{xtype: 'MyApp.views.MainMenu', id: 'mainMenu'}
			]
		});
		MyApp.views.Viewport.superclass.initComponent.apply(this, arguments);
	},
	
	reveal: function(target){
		var direction = (target === 'userLogin')? 'right' : 'left'
		this.setActiveItem(
			App.views[target],
			{ type: 'slide', direction: direction}
		);
	}
});