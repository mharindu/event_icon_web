Ext.define('MyApp.view.PreviousEventList', {
    extend: 'Ext.List',
	alias: "widget.previouseventlist",
	xtype: 'previouseventlist',
	
	config: {
		//title: 'Participant List',
		//grouped: true,
		itemTpl: "<div style='float:left;width:60px'><img src='resources/images/user.png' width='30px' height='30px'/></div><div style='margin-left:62px'>{event_name} ({year})</div>",
		store: 'PastEvents',
		onItemDisclosure: true,
    },
	
	
});