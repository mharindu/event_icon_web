Ext.define('MyApp.view.AboutUs', {
    extend: 'Ext.Panel',
	alias: "widget.aboutus",
	
	config: {
		items: [
		
			{
                xtype: 'titlebar',
                docked: 'top',
                title: 'About Us',
                items: [
                    {
                        xtype: 'button',
                        itemId: 'backButton',
                        ui: 'back',
                        
                        iconCls: 'arrow_left',
                        text: ''
                    }
                ]
            },
		  
			{
				xtype: 'image',
				src: 'resources/images/ucsc_logo.png',
				style: 'width:120px;height:100px;margin:auto'
			},
		   {
			html: 'This is a 3<sup>rd</sup> year group project ',
			style: 'text-align:center;margin-top:15px'
		  } 
		  
		
		   
		   
		],
		listeners: [{
            delegate: '#backButton',
            event: 'tap',
            fn: 'onBackButtonTap'
        }]
    },
	
	onBackButtonTap: function() {
	  var me = this;
	  me.fireEvent('backCommand', me);
	}
});