Ext.define('MyApp.view.UserDetail', {
    extend: 'Ext.Panel',
	alias: "widget.userdetail",
	
	config: {
		
		styleHtmlContent: true,
		scrollable: 'vertical',
		tpl: 'Hello, {firstName}',
		
    },
	
});