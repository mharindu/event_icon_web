Ext.define('MyApp.view.MessagesList', {
    extend: 'Ext.List',
	alias: "widget.messageslist",
	xtype: 'messageslist',
	
	config: {
		title: 'Message List',
		itemTpl: "<div style='float:left;width:60px'><img src='resources/images/mail.png' width='30px' height='30px'/></div><div style='margin-left:62px'>{subject}: {sender_id} to {receiver_id}</div>",
		store: 'UserUsers',
		onItemDisclosure: true,
    },
	
	
});

