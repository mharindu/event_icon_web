Ext.define('MyApp.view.MessageDetail', {
    extend: 'Ext.Panel',
    alias: "widget.messagedetail",
    requires: ['Ext.MessageBox'],
	
	config: {
            items: [
                {
                    xtype: 'label',
                    scrollable: 'vertical',
                    tpl: '{message}',
                },
                
                {
                    xtype: 'textareafield',
                    label: 'MessageBody',
                         
                },
                {
                     xtype: 'button',
                     text: 'send',
                     ui: 'action',
                     itemId: 'sendButton'
                     
                }
                
            ],
            
            listeners: [{
                delegate: '#sendButton',
                event: 'tap',
                fn: 'onSendButtonTap'
            }]
		
		
    },
    
    onSendButtonTap: function(){
        Ext.Msg.alert('Confirmation', 'Message Sent', Ext.emptyFn);
        
    }
	
});
