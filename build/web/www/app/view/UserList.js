Ext.define('MyApp.view.UserList', {
    extend: 'Ext.List',
	alias: "widget.userlist",
	xtype: 'userlist',
	
	config: {
		title: 'Participant List',
		grouped: true,
		itemTpl: "<div style='float:left;width:60px'><img src='resources/images/user.png' width='30px' height='30px'/></div><div style='margin-left:62px'>{first_name} {last_name}</div>",
		store: 'Users',
		onItemDisclosure: true,
    },
	
	
});