Ext.define('MyApp.model.Experience', {
    extend: 'Ext.data.Model',
	alias: "widget.experience",
	
	config: {
		  fields: ['user_id','experience_id','title','description','year']
    },
	
	
});