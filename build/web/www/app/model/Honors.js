Ext.define('MyApp.model.Honors', {
    extend: 'Ext.data.Model',
	alias: "widget.honors",
	
	config: {
		  fields: ['user_id','honor_id','title','description','year']
    },
	
	
});