Ext.define('MyApp.model.User', {
    extend: 'Ext.data.Model',
	alias: "widget.user",
	
	config: {
		  fields: ['user_id','first_name','last_name','other_names','user_name','password','profile_pic']
    },
	
	
});