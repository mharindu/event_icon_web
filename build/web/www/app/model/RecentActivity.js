Ext.define('MyApp.model.RecentActivity', {
    extend: 'Ext.data.Model',
	alias: "widget.recentactivity",
	
	config: {
		  fields: ['user_id','act_id','event_name','description']
    },
	
	
});