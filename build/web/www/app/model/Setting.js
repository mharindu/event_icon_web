Ext.define('MyApp.model.Setting', {
    extend: 'Ext.data.Model',
	alias: "widget.setting",
	
	config: {
		  fields: ['user_id','setting_id','visibilty','bluetooth_visibility','pub_profile_visibilty','social_profile_visibility','bluetooth_MAC']
    },
	
	
});