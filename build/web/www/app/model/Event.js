Ext.define('MyApp.model.Event', {
    extend: 'Ext.data.Model',
	alias: "widget.event",
	
	config: {
		  fields: ['event_id','org_id','event_name','description','type','start_time','end_time','date','address_L1','address_L2','address_L3','country','ticket_des','profile_pic']
    },
	
	
});