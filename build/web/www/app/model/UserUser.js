Ext.define('MyApp.model.UserUser', {
    extend: 'Ext.data.Model',
	alias: "widget.useruser",
	
	config: {
		  fields: ['message_id','event_id','sender_id','receiver_id','subject','message','state','time_stamp']
    },
	
	
});