Ext.define('MyApp.model.PublicProfile', {
    extend: 'Ext.data.Model',
	alias: "widget.publicprofile",
	
	config: {
		  fields: ['user_id','profile_id','xml_path']
    },
	
	
});