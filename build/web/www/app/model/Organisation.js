Ext.define('MyApp.model.Organisation', {
    extend: 'Ext.data.Model',
	alias: "widget.organisation",
	
	config: {
		  fields: ['user_id','organization_id','title','description','start_time','end_time']
    },
	
	
});