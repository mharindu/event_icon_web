Ext.define('MyApp.model.AgendaElement', {
   	extend: 'Ext.data.Model',
	alias: "widget.agendaelement",
	
	config: {
		fields: ['event_id','element_id','title','presenter','description','start_time','end_time']
    },
});	