Ext.define('MyApp.model.UserOrganizer', {
    extend: 'Ext.data.Model',
	alias: "widget.userorganizer",
	
	config: {
		  fields: ['message_id','event_id','sender_id','receiver_id','subject','message','state','time_stamp']
    },
	
	
});