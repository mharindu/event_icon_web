Ext.define('MyApp.model.Project', {
    extend: 'Ext.data.Model',
	alias: "widget.project",
	
	config: {
		  fields: ['user_id','project_id','project_name','description','year']
    },
	
	
});