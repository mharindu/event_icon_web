Ext.define('MyApp.model.BusinessCard', {
    extend: 'Ext.data.Model',
    alias: "widget.businesscard",
	
	config: {
		  fields: ['user_id','address_L1','address_L2','address_L3','contact_number_1','contact_number_2','country','designation','email','organisation','website','profile_summary']
    },
	
	
});