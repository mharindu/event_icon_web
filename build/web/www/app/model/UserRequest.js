Ext.define('MyApp.model.UserRequest', {
    extend: 'Ext.data.Model',
	alias: "widget.userrequest",
	
	config: {
		  fields: ['request_id','user_id','event_id']
    },
	
	
});