Ext.define('MyApp.model.Material', {
    extend: 'Ext.data.Model',
	alias: "widget.material",
	
	config: {
		  fields: ['event_id','material_id','xml_path','description','tag_1','tag_2','tag_3']
    },
	
	
});