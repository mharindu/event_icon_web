Ext.define('MyApp.model.PastEvent', {
    extend: 'Ext.data.Model',
	alias: "widget.pastevent",
	
	config: {
		  fields: ['event_id','org_id','event_name','description','year']
    },
	
	
});