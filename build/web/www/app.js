/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".
 
 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */

// DO NOT DELETE - this directive is required for Sencha Cmd packages to work.
//@require @packageOverrides

//<debug>
Ext.Loader.setConfig({
});

Ext.application({
    views: [
        'UserLogin',
        'MainMenu',
        'Profile',
        'Events',
        'Settings',
        'OnAir',
        'AboutUs',
        'UserList',
        'UserDetail',
        'UserNavi',
        'MyProfile',
        'UpEventList',
        'UpEventNavi',
        'PreviousEventList',
        'MessagesNavi',
        'MessagesList',
        'MessageDetail'
    ],
    controllers: [
        'Main'
    ],
    stores: [
         'Users', 'ProfileStore', 'AEvents' , 'BusinessCards', 'EventUsers', 'PastEvents','UserUsers'
    ],
    models: [
        'User', 'BusinessCard', 'AdminMessage', 'AgendaElement', 'Event', 'EventUser','PastEvent', 'Experience', 'Honors', 'Material', 'Organisation', 'Organizer', 'Project','PublicProfile','RecentActivity','Setting','UserOrganizer','UserRequest','UserUser'
    ],
    name: 'MyApp',
    launch: function() {

        Ext.Viewport.add([
            {xtype: 'userlogin'},
            {xtype: 'mainmenu'},
        ]);
    }

});