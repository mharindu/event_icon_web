/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import com.sun.jersey.api.json.JSONWithPadding;
import entity.User;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Harindu
 */
@Stateless
@Path("user")
public class UserFacadeREST extends AbstractFacade<User> {
    @PersistenceContext(unitName = "event_icon2PU")
    private EntityManager em;

    public UserFacadeREST() {
        super(User.class);
    }

    @POST
    @Override
    @Consumes({"application/json"})
    public void create(User entity) {
        super.create(entity);
    }

    @PUT
    @Override
    @Consumes({"application/json"})
    public void edit(User entity) {
        super.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Short id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public User find(@PathParam("id") Short id) {
        
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<User> findAll() {
        return super.findAll();
    }
    
//    @GET
//    @Path("{id}")
//    @Produces({"application/x-javascript"})
//    public JSONWithPadding getJson(@QueryParam("callback") String callback,@PathParam("id") Short id) throws Exception {
//        JSONObject user = new JSONObject();
//        User person = super.find(id);
//        
//        JSONObject obj = new JSONObject();
//
//            
//            obj.put("firstName", person.getFirstName());
//            obj.put("lastName", person.getLastName());
//            obj.put("otherNames", person.getOtherNames());
//            obj.put("userName", person.getUserName());
//            obj.put("profilePic", person.getProfilePic());
//            user.accumulate("user", obj);
//        
//        System.out.println(user.toString());
//        return new JSONWithPadding(user, callback);
//}

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<User> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }
    
}
