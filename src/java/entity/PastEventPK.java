/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class PastEventPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "org_id")
    private short orgId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;

    public PastEventPK() {
    }

    public PastEventPK(short orgId, short eventId) {
        this.orgId = orgId;
        this.eventId = eventId;
    }

    public short getOrgId() {
        return orgId;
    }

    public void setOrgId(short orgId) {
        this.orgId = orgId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) orgId;
        hash += (int) eventId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PastEventPK)) {
            return false;
        }
        PastEventPK other = (PastEventPK) object;
        if (this.orgId != other.orgId) {
            return false;
        }
        if (this.eventId != other.eventId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PastEventPK[ orgId=" + orgId + ", eventId=" + eventId + " ]";
    }
    
}
