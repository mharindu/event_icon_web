/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class RecentActivityPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "act_id")
    private short actId;

    public RecentActivityPK() {
    }

    public RecentActivityPK(short userId, short actId) {
        this.userId = userId;
        this.actId = actId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getActId() {
        return actId;
    }

    public void setActId(short actId) {
        this.actId = actId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) actId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecentActivityPK)) {
            return false;
        }
        RecentActivityPK other = (RecentActivityPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.actId != other.actId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RecentActivityPK[ userId=" + userId + ", actId=" + actId + " ]";
    }
    
}
