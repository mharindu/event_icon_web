/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "honors")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Honors.findAll", query = "SELECT h FROM Honors h"),
    @NamedQuery(name = "Honors.findByUserId", query = "SELECT h FROM Honors h WHERE h.honorsPK.userId = :userId"),
    @NamedQuery(name = "Honors.findByHonorId", query = "SELECT h FROM Honors h WHERE h.honorsPK.honorId = :honorId"),
    @NamedQuery(name = "Honors.findByTitle", query = "SELECT h FROM Honors h WHERE h.title = :title"),
    @NamedQuery(name = "Honors.findByDescription", query = "SELECT h FROM Honors h WHERE h.description = :description"),
    @NamedQuery(name = "Honors.findByYear", query = "SELECT h FROM Honors h WHERE h.year = :year")})
public class Honors implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HonorsPK honorsPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    @Temporal(TemporalType.DATE)
    private Date year;

    public Honors() {
    }

    public Honors(HonorsPK honorsPK) {
        this.honorsPK = honorsPK;
    }

    public Honors(HonorsPK honorsPK, String title, String description, Date year) {
        this.honorsPK = honorsPK;
        this.title = title;
        this.description = description;
        this.year = year;
    }

    public Honors(short userId, short honorId) {
        this.honorsPK = new HonorsPK(userId, honorId);
    }

    public HonorsPK getHonorsPK() {
        return honorsPK;
    }

    public void setHonorsPK(HonorsPK honorsPK) {
        this.honorsPK = honorsPK;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (honorsPK != null ? honorsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Honors)) {
            return false;
        }
        Honors other = (Honors) object;
        if ((this.honorsPK == null && other.honorsPK != null) || (this.honorsPK != null && !this.honorsPK.equals(other.honorsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Honors[ honorsPK=" + honorsPK + " ]";
    }
    
}
