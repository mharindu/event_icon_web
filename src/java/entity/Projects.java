/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "projects")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Projects.findAll", query = "SELECT p FROM Projects p"),
    @NamedQuery(name = "Projects.findByUserId", query = "SELECT p FROM Projects p WHERE p.projectsPK.userId = :userId"),
    @NamedQuery(name = "Projects.findByProjectId", query = "SELECT p FROM Projects p WHERE p.projectsPK.projectId = :projectId"),
    @NamedQuery(name = "Projects.findByProjectName", query = "SELECT p FROM Projects p WHERE p.projectName = :projectName"),
    @NamedQuery(name = "Projects.findByDescription", query = "SELECT p FROM Projects p WHERE p.description = :description"),
    @NamedQuery(name = "Projects.findByYear", query = "SELECT p FROM Projects p WHERE p.year = :year")})
public class Projects implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProjectsPK projectsPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "project_name")
    private String projectName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    @Temporal(TemporalType.DATE)
    private Date year;

    public Projects() {
    }

    public Projects(ProjectsPK projectsPK) {
        this.projectsPK = projectsPK;
    }

    public Projects(ProjectsPK projectsPK, String projectName, String description, Date year) {
        this.projectsPK = projectsPK;
        this.projectName = projectName;
        this.description = description;
        this.year = year;
    }

    public Projects(short userId, short projectId) {
        this.projectsPK = new ProjectsPK(userId, projectId);
    }

    public ProjectsPK getProjectsPK() {
        return projectsPK;
    }

    public void setProjectsPK(ProjectsPK projectsPK) {
        this.projectsPK = projectsPK;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (projectsPK != null ? projectsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Projects)) {
            return false;
        }
        Projects other = (Projects) object;
        if ((this.projectsPK == null && other.projectsPK != null) || (this.projectsPK != null && !this.projectsPK.equals(other.projectsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Projects[ projectsPK=" + projectsPK + " ]";
    }
    
}
