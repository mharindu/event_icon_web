/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "user_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserUser.findAll", query = "SELECT u FROM UserUser u"),
    @NamedQuery(name = "UserUser.findByMessageId", query = "SELECT u FROM UserUser u WHERE u.messageId = :messageId"),
    @NamedQuery(name = "UserUser.findByEventId", query = "SELECT u FROM UserUser u WHERE u.eventId = :eventId"),
    @NamedQuery(name = "UserUser.findBySenderId", query = "SELECT u FROM UserUser u WHERE u.senderId = :senderId"),
    @NamedQuery(name = "UserUser.findByReceiverId", query = "SELECT u FROM UserUser u WHERE u.receiverId = :receiverId"),
    @NamedQuery(name = "UserUser.findBySubject", query = "SELECT u FROM UserUser u WHERE u.subject = :subject"),
    @NamedQuery(name = "UserUser.findByMessage", query = "SELECT u FROM UserUser u WHERE u.message = :message"),
    @NamedQuery(name = "UserUser.findByState", query = "SELECT u FROM UserUser u WHERE u.state = :state"),
    @NamedQuery(name = "UserUser.findByTimeStamp", query = "SELECT u FROM UserUser u WHERE u.timeStamp = :timeStamp")})
public class UserUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "message_id")
    private Short messageId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "sender_id")
    private short senderId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "receiver_id")
    private short receiverId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "subject")
    private String subject;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Column(name = "state")
    private int state;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_stamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp;

    public UserUser() {
    }

    public UserUser(Short messageId) {
        this.messageId = messageId;
    }

    public UserUser(Short messageId, short eventId, short senderId, short receiverId, String subject, String message, int state, Date timeStamp) {
        this.messageId = messageId;
        this.eventId = eventId;
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.subject = subject;
        this.message = message;
        this.state = state;
        this.timeStamp = timeStamp;
    }

    public Short getMessageId() {
        return messageId;
    }

    public void setMessageId(Short messageId) {
        this.messageId = messageId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    public short getSenderId() {
        return senderId;
    }

    public void setSenderId(short senderId) {
        this.senderId = senderId;
    }

    public short getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(short receiverId) {
        this.receiverId = receiverId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (messageId != null ? messageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserUser)) {
            return false;
        }
        UserUser other = (UserUser) object;
        if ((this.messageId == null && other.messageId != null) || (this.messageId != null && !this.messageId.equals(other.messageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserUser[ messageId=" + messageId + " ]";
    }
    
}
