/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "organizer")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Organizer.findAll", query = "SELECT o FROM Organizer o"),
    @NamedQuery(name = "Organizer.findByOrgId", query = "SELECT o FROM Organizer o WHERE o.orgId = :orgId"),
    @NamedQuery(name = "Organizer.findByName", query = "SELECT o FROM Organizer o WHERE o.name = :name"),
    @NamedQuery(name = "Organizer.findByAddressL1", query = "SELECT o FROM Organizer o WHERE o.addressL1 = :addressL1"),
    @NamedQuery(name = "Organizer.findByAddressL2", query = "SELECT o FROM Organizer o WHERE o.addressL2 = :addressL2"),
    @NamedQuery(name = "Organizer.findByAddressL3", query = "SELECT o FROM Organizer o WHERE o.addressL3 = :addressL3"),
    @NamedQuery(name = "Organizer.findByCountry", query = "SELECT o FROM Organizer o WHERE o.country = :country"),
    @NamedQuery(name = "Organizer.findByContactNumber1", query = "SELECT o FROM Organizer o WHERE o.contactNumber1 = :contactNumber1"),
    @NamedQuery(name = "Organizer.findByContactNumber2", query = "SELECT o FROM Organizer o WHERE o.contactNumber2 = :contactNumber2"),
    @NamedQuery(name = "Organizer.findByEmail", query = "SELECT o FROM Organizer o WHERE o.email = :email"),
    @NamedQuery(name = "Organizer.findByWebsite", query = "SELECT o FROM Organizer o WHERE o.website = :website"),
    @NamedQuery(name = "Organizer.findByUserName", query = "SELECT o FROM Organizer o WHERE o.userName = :userName"),
    @NamedQuery(name = "Organizer.findByPassword", query = "SELECT o FROM Organizer o WHERE o.password = :password")})
public class Organizer implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "org_id")
    private Short orgId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "address_L1")
    private String addressL1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "address_L2")
    private String addressL2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "address_L3")
    private String addressL3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "country")
    private String country;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "contact_number_1")
    private String contactNumber1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "contact_number_2")
    private String contactNumber2;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "website")
    private String website;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "user_name")
    private String userName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 256)
    @Column(name = "password")
    private String password;

    public Organizer() {
    }

    public Organizer(Short orgId) {
        this.orgId = orgId;
    }

    public Organizer(Short orgId, String name, String addressL1, String addressL2, String addressL3, String country, String contactNumber1, String contactNumber2, String email, String website, String userName, String password) {
        this.orgId = orgId;
        this.name = name;
        this.addressL1 = addressL1;
        this.addressL2 = addressL2;
        this.addressL3 = addressL3;
        this.country = country;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.email = email;
        this.website = website;
        this.userName = userName;
        this.password = password;
    }

    public Short getOrgId() {
        return orgId;
    }

    public void setOrgId(Short orgId) {
        this.orgId = orgId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressL1() {
        return addressL1;
    }

    public void setAddressL1(String addressL1) {
        this.addressL1 = addressL1;
    }

    public String getAddressL2() {
        return addressL2;
    }

    public void setAddressL2(String addressL2) {
        this.addressL2 = addressL2;
    }

    public String getAddressL3() {
        return addressL3;
    }

    public void setAddressL3(String addressL3) {
        this.addressL3 = addressL3;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getContactNumber1() {
        return contactNumber1;
    }

    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    public String getContactNumber2() {
        return contactNumber2;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orgId != null ? orgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organizer)) {
            return false;
        }
        Organizer other = (Organizer) object;
        if ((this.orgId == null && other.orgId != null) || (this.orgId != null && !this.orgId.equals(other.orgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Organizer[ orgId=" + orgId + " ]";
    }
    
}
