/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "event_user")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "EventUser.findAll", query = "SELECT e FROM EventUser e"),
    @NamedQuery(name = "EventUser.findByEventId", query = "SELECT e FROM EventUser e WHERE e.eventUserPK.eventId = :eventId"),
    @NamedQuery(name = "EventUser.findByUserId", query = "SELECT e FROM EventUser e WHERE e.eventUserPK.userId = :userId"),
    @NamedQuery(name = "EventUser.findByVerify", query = "SELECT e FROM EventUser e WHERE e.verify = :verify")})
public class EventUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected EventUserPK eventUserPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "verify")
    private short verify;

    public EventUser() {
    }

    public EventUser(EventUserPK eventUserPK) {
        this.eventUserPK = eventUserPK;
    }

    public EventUser(EventUserPK eventUserPK, short verify) {
        this.eventUserPK = eventUserPK;
        this.verify = verify;
    }

    public EventUser(short eventId, short userId) {
        this.eventUserPK = new EventUserPK(eventId, userId);
    }

    public EventUserPK getEventUserPK() {
        return eventUserPK;
    }

    public void setEventUserPK(EventUserPK eventUserPK) {
        this.eventUserPK = eventUserPK;
    }

    public short getVerify() {
        return verify;
    }

    public void setVerify(short verify) {
        this.verify = verify;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (eventUserPK != null ? eventUserPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventUser)) {
            return false;
        }
        EventUser other = (EventUser) object;
        if ((this.eventUserPK == null && other.eventUserPK != null) || (this.eventUserPK != null && !this.eventUserPK.equals(other.eventUserPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.EventUser[ eventUserPK=" + eventUserPK + " ]";
    }
    
}
