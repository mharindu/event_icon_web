/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "settings")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Settings.findAll", query = "SELECT s FROM Settings s"),
    @NamedQuery(name = "Settings.findByUserId", query = "SELECT s FROM Settings s WHERE s.settingsPK.userId = :userId"),
    @NamedQuery(name = "Settings.findBySettingId", query = "SELECT s FROM Settings s WHERE s.settingsPK.settingId = :settingId"),
    @NamedQuery(name = "Settings.findByVisibilty", query = "SELECT s FROM Settings s WHERE s.visibilty = :visibilty"),
    @NamedQuery(name = "Settings.findByBluetoothVisibility", query = "SELECT s FROM Settings s WHERE s.bluetoothVisibility = :bluetoothVisibility"),
    @NamedQuery(name = "Settings.findByPubProfileVisibility", query = "SELECT s FROM Settings s WHERE s.pubProfileVisibility = :pubProfileVisibility"),
    @NamedQuery(name = "Settings.findBySocialProfileVisibility", query = "SELECT s FROM Settings s WHERE s.socialProfileVisibility = :socialProfileVisibility"),
    @NamedQuery(name = "Settings.findByBluetoothMAC", query = "SELECT s FROM Settings s WHERE s.bluetoothMAC = :bluetoothMAC")})
public class Settings implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SettingsPK settingsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "visibilty")
    private int visibilty;
    @Basic(optional = false)
    @NotNull
    @Column(name = "bluetooth_visibility")
    private int bluetoothVisibility;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pub_profile_visibility")
    private int pubProfileVisibility;
    @Basic(optional = false)
    @NotNull
    @Column(name = "social_profile_visibility")
    private int socialProfileVisibility;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "bluetooth_MAC")
    private String bluetoothMAC;

    public Settings() {
    }

    public Settings(SettingsPK settingsPK) {
        this.settingsPK = settingsPK;
    }

    public Settings(SettingsPK settingsPK, int visibilty, int bluetoothVisibility, int pubProfileVisibility, int socialProfileVisibility, String bluetoothMAC) {
        this.settingsPK = settingsPK;
        this.visibilty = visibilty;
        this.bluetoothVisibility = bluetoothVisibility;
        this.pubProfileVisibility = pubProfileVisibility;
        this.socialProfileVisibility = socialProfileVisibility;
        this.bluetoothMAC = bluetoothMAC;
    }

    public Settings(short userId, short settingId) {
        this.settingsPK = new SettingsPK(userId, settingId);
    }

    public SettingsPK getSettingsPK() {
        return settingsPK;
    }

    public void setSettingsPK(SettingsPK settingsPK) {
        this.settingsPK = settingsPK;
    }

    public int getVisibilty() {
        return visibilty;
    }

    public void setVisibilty(int visibilty) {
        this.visibilty = visibilty;
    }

    public int getBluetoothVisibility() {
        return bluetoothVisibility;
    }

    public void setBluetoothVisibility(int bluetoothVisibility) {
        this.bluetoothVisibility = bluetoothVisibility;
    }

    public int getPubProfileVisibility() {
        return pubProfileVisibility;
    }

    public void setPubProfileVisibility(int pubProfileVisibility) {
        this.pubProfileVisibility = pubProfileVisibility;
    }

    public int getSocialProfileVisibility() {
        return socialProfileVisibility;
    }

    public void setSocialProfileVisibility(int socialProfileVisibility) {
        this.socialProfileVisibility = socialProfileVisibility;
    }

    public String getBluetoothMAC() {
        return bluetoothMAC;
    }

    public void setBluetoothMAC(String bluetoothMAC) {
        this.bluetoothMAC = bluetoothMAC;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (settingsPK != null ? settingsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Settings)) {
            return false;
        }
        Settings other = (Settings) object;
        if ((this.settingsPK == null && other.settingsPK != null) || (this.settingsPK != null && !this.settingsPK.equals(other.settingsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Settings[ settingsPK=" + settingsPK + " ]";
    }
    
}
