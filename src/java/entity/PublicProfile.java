/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "public_profile")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PublicProfile.findAll", query = "SELECT p FROM PublicProfile p"),
    @NamedQuery(name = "PublicProfile.findByUserId", query = "SELECT p FROM PublicProfile p WHERE p.publicProfilePK.userId = :userId"),
    @NamedQuery(name = "PublicProfile.findByProfileId", query = "SELECT p FROM PublicProfile p WHERE p.publicProfilePK.profileId = :profileId"),
    @NamedQuery(name = "PublicProfile.findByXmlPath", query = "SELECT p FROM PublicProfile p WHERE p.xmlPath = :xmlPath")})
public class PublicProfile implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PublicProfilePK publicProfilePK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "xml_path")
    private String xmlPath;

    public PublicProfile() {
    }

    public PublicProfile(PublicProfilePK publicProfilePK) {
        this.publicProfilePK = publicProfilePK;
    }

    public PublicProfile(PublicProfilePK publicProfilePK, String xmlPath) {
        this.publicProfilePK = publicProfilePK;
        this.xmlPath = xmlPath;
    }

    public PublicProfile(short userId, short profileId) {
        this.publicProfilePK = new PublicProfilePK(userId, profileId);
    }

    public PublicProfilePK getPublicProfilePK() {
        return publicProfilePK;
    }

    public void setPublicProfilePK(PublicProfilePK publicProfilePK) {
        this.publicProfilePK = publicProfilePK;
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (publicProfilePK != null ? publicProfilePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PublicProfile)) {
            return false;
        }
        PublicProfile other = (PublicProfile) object;
        if ((this.publicProfilePK == null && other.publicProfilePK != null) || (this.publicProfilePK != null && !this.publicProfilePK.equals(other.publicProfilePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PublicProfile[ publicProfilePK=" + publicProfilePK + " ]";
    }
    
}
