/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class AgendaElementPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "element_id")
    private short elementId;

    public AgendaElementPK() {
    }

    public AgendaElementPK(short eventId, short elementId) {
        this.eventId = eventId;
        this.elementId = elementId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    public short getElementId() {
        return elementId;
    }

    public void setElementId(short elementId) {
        this.elementId = elementId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) elementId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgendaElementPK)) {
            return false;
        }
        AgendaElementPK other = (AgendaElementPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.elementId != other.elementId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AgendaElementPK[ eventId=" + eventId + ", elementId=" + elementId + " ]";
    }
    
}
