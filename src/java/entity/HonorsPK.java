/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class HonorsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "honor_id")
    private short honorId;

    public HonorsPK() {
    }

    public HonorsPK(short userId, short honorId) {
        this.userId = userId;
        this.honorId = honorId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getHonorId() {
        return honorId;
    }

    public void setHonorId(short honorId) {
        this.honorId = honorId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) honorId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HonorsPK)) {
            return false;
        }
        HonorsPK other = (HonorsPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.honorId != other.honorId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.HonorsPK[ userId=" + userId + ", honorId=" + honorId + " ]";
    }
    
}
