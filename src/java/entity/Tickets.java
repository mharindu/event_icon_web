/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "tickets")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tickets.findAll", query = "SELECT t FROM Tickets t"),
    @NamedQuery(name = "Tickets.findByEventId", query = "SELECT t FROM Tickets t WHERE t.ticketsPK.eventId = :eventId"),
    @NamedQuery(name = "Tickets.findByTicketId", query = "SELECT t FROM Tickets t WHERE t.ticketsPK.ticketId = :ticketId"),
    @NamedQuery(name = "Tickets.findByTicketPrice", query = "SELECT t FROM Tickets t WHERE t.ticketPrice = :ticketPrice"),
    @NamedQuery(name = "Tickets.findByNoIssued", query = "SELECT t FROM Tickets t WHERE t.noIssued = :noIssued")})
public class Tickets implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TicketsPK ticketsPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticket_price")
    private int ticketPrice;
    @Basic(optional = false)
    @NotNull
    @Column(name = "no_issued")
    private int noIssued;

    public Tickets() {
    }

    public Tickets(TicketsPK ticketsPK) {
        this.ticketsPK = ticketsPK;
    }

    public Tickets(TicketsPK ticketsPK, int ticketPrice, int noIssued) {
        this.ticketsPK = ticketsPK;
        this.ticketPrice = ticketPrice;
        this.noIssued = noIssued;
    }

    public Tickets(short eventId, short ticketId) {
        this.ticketsPK = new TicketsPK(eventId, ticketId);
    }

    public TicketsPK getTicketsPK() {
        return ticketsPK;
    }

    public void setTicketsPK(TicketsPK ticketsPK) {
        this.ticketsPK = ticketsPK;
    }

    public int getTicketPrice() {
        return ticketPrice;
    }

    public void setTicketPrice(int ticketPrice) {
        this.ticketPrice = ticketPrice;
    }

    public int getNoIssued() {
        return noIssued;
    }

    public void setNoIssued(int noIssued) {
        this.noIssued = noIssued;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ticketsPK != null ? ticketsPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Tickets)) {
            return false;
        }
        Tickets other = (Tickets) object;
        if ((this.ticketsPK == null && other.ticketsPK != null) || (this.ticketsPK != null && !this.ticketsPK.equals(other.ticketsPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Tickets[ ticketsPK=" + ticketsPK + " ]";
    }
    
}
