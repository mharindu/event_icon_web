/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "material")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Material.findAll", query = "SELECT m FROM Material m"),
    @NamedQuery(name = "Material.findByEventId", query = "SELECT m FROM Material m WHERE m.materialPK.eventId = :eventId"),
    @NamedQuery(name = "Material.findByMaterialId", query = "SELECT m FROM Material m WHERE m.materialPK.materialId = :materialId"),
    @NamedQuery(name = "Material.findByXmlPath", query = "SELECT m FROM Material m WHERE m.xmlPath = :xmlPath"),
    @NamedQuery(name = "Material.findByDescription", query = "SELECT m FROM Material m WHERE m.description = :description"),
    @NamedQuery(name = "Material.findByTag1", query = "SELECT m FROM Material m WHERE m.tag1 = :tag1"),
    @NamedQuery(name = "Material.findByTag2", query = "SELECT m FROM Material m WHERE m.tag2 = :tag2"),
    @NamedQuery(name = "Material.findByTag3", query = "SELECT m FROM Material m WHERE m.tag3 = :tag3")})
public class Material implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MaterialPK materialPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "xml_path")
    private String xmlPath;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "tag_1")
    private String tag1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "tag_2")
    private String tag2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "tag_3")
    private String tag3;

    public Material() {
    }

    public Material(MaterialPK materialPK) {
        this.materialPK = materialPK;
    }

    public Material(MaterialPK materialPK, String xmlPath, String description, String tag1, String tag2, String tag3) {
        this.materialPK = materialPK;
        this.xmlPath = xmlPath;
        this.description = description;
        this.tag1 = tag1;
        this.tag2 = tag2;
        this.tag3 = tag3;
    }

    public Material(short eventId, short materialId) {
        this.materialPK = new MaterialPK(eventId, materialId);
    }

    public MaterialPK getMaterialPK() {
        return materialPK;
    }

    public void setMaterialPK(MaterialPK materialPK) {
        this.materialPK = materialPK;
    }

    public String getXmlPath() {
        return xmlPath;
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTag1() {
        return tag1;
    }

    public void setTag1(String tag1) {
        this.tag1 = tag1;
    }

    public String getTag2() {
        return tag2;
    }

    public void setTag2(String tag2) {
        this.tag2 = tag2;
    }

    public String getTag3() {
        return tag3;
    }

    public void setTag3(String tag3) {
        this.tag3 = tag3;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (materialPK != null ? materialPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Material)) {
            return false;
        }
        Material other = (Material) object;
        if ((this.materialPK == null && other.materialPK != null) || (this.materialPK != null && !this.materialPK.equals(other.materialPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Material[ materialPK=" + materialPK + " ]";
    }
    
}
