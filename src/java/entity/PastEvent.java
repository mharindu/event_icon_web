/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "past_event")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PastEvent.findAll", query = "SELECT p FROM PastEvent p"),
    @NamedQuery(name = "PastEvent.findByOrgId", query = "SELECT p FROM PastEvent p WHERE p.pastEventPK.orgId = :orgId"),
    @NamedQuery(name = "PastEvent.findByEventId", query = "SELECT p FROM PastEvent p WHERE p.pastEventPK.eventId = :eventId"),
    @NamedQuery(name = "PastEvent.findByEventName", query = "SELECT p FROM PastEvent p WHERE p.eventName = :eventName"),
    @NamedQuery(name = "PastEvent.findByDescription", query = "SELECT p FROM PastEvent p WHERE p.description = :description"),
    @NamedQuery(name = "PastEvent.findByYear", query = "SELECT p FROM PastEvent p WHERE p.year = :year")})
public class PastEvent implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PastEventPK pastEventPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "event_name")
    private String eventName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "description")
    private String description;
    @Column(name = "year")
    @Temporal(TemporalType.DATE)
    private Date year;

    public PastEvent() {
    }

    public PastEvent(PastEventPK pastEventPK) {
        this.pastEventPK = pastEventPK;
    }

    public PastEvent(PastEventPK pastEventPK, String eventName, String description) {
        this.pastEventPK = pastEventPK;
        this.eventName = eventName;
        this.description = description;
    }

    public PastEvent(short orgId, short eventId) {
        this.pastEventPK = new PastEventPK(orgId, eventId);
    }

    public PastEventPK getPastEventPK() {
        return pastEventPK;
    }

    public void setPastEventPK(PastEventPK pastEventPK) {
        this.pastEventPK = pastEventPK;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pastEventPK != null ? pastEventPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PastEvent)) {
            return false;
        }
        PastEvent other = (PastEvent) object;
        if ((this.pastEventPK == null && other.pastEventPK != null) || (this.pastEventPK != null && !this.pastEventPK.equals(other.pastEventPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PastEvent[ pastEventPK=" + pastEventPK + " ]";
    }
    
}
