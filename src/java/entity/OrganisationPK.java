/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class OrganisationPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "organization_id")
    private short organizationId;

    public OrganisationPK() {
    }

    public OrganisationPK(short userId, short organizationId) {
        this.userId = userId;
        this.organizationId = organizationId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(short organizationId) {
        this.organizationId = organizationId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) organizationId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OrganisationPK)) {
            return false;
        }
        OrganisationPK other = (OrganisationPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.organizationId != other.organizationId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.OrganisationPK[ userId=" + userId + ", organizationId=" + organizationId + " ]";
    }
    
}
