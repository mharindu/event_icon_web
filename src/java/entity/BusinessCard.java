/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "business_card")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BusinessCard.findAll", query = "SELECT b FROM BusinessCard b"),
    @NamedQuery(name = "BusinessCard.findByUserId", query = "SELECT b FROM BusinessCard b WHERE b.userId = :userId"),
    @NamedQuery(name = "BusinessCard.findByDesignation", query = "SELECT b FROM BusinessCard b WHERE b.designation = :designation"),
    @NamedQuery(name = "BusinessCard.findByOrganisation", query = "SELECT b FROM BusinessCard b WHERE b.organisation = :organisation"),
    @NamedQuery(name = "BusinessCard.findByEmail", query = "SELECT b FROM BusinessCard b WHERE b.email = :email"),
    @NamedQuery(name = "BusinessCard.findByContactNumber1", query = "SELECT b FROM BusinessCard b WHERE b.contactNumber1 = :contactNumber1"),
    @NamedQuery(name = "BusinessCard.findByContactNumber2", query = "SELECT b FROM BusinessCard b WHERE b.contactNumber2 = :contactNumber2"),
    @NamedQuery(name = "BusinessCard.findByProfileSummary", query = "SELECT b FROM BusinessCard b WHERE b.profileSummary = :profileSummary"),
    @NamedQuery(name = "BusinessCard.findByWebsite", query = "SELECT b FROM BusinessCard b WHERE b.website = :website"),
    @NamedQuery(name = "BusinessCard.findByAddressL1", query = "SELECT b FROM BusinessCard b WHERE b.addressL1 = :addressL1"),
    @NamedQuery(name = "BusinessCard.findByAddressL2", query = "SELECT b FROM BusinessCard b WHERE b.addressL2 = :addressL2"),
    @NamedQuery(name = "BusinessCard.findByAddressL3", query = "SELECT b FROM BusinessCard b WHERE b.addressL3 = :addressL3"),
    @NamedQuery(name = "BusinessCard.findByCountry", query = "SELECT b FROM BusinessCard b WHERE b.country = :country")})
public class BusinessCard implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private Short userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "designation")
    private String designation;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "organisation")
    private String organisation;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "contact_number_1")
    private String contactNumber1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "contact_number_2")
    private String contactNumber2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "profile_summary")
    private String profileSummary;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "website")
    private String website;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "address_L1")
    private String addressL1;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "address_L2")
    private String addressL2;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "address_L3")
    private String addressL3;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "country")
    private String country;

    public BusinessCard() {
    }

    public BusinessCard(Short userId) {
        this.userId = userId;
    }

    public BusinessCard(Short userId, String designation, String organisation, String email, String contactNumber1, String contactNumber2, String profileSummary, String website, String addressL1, String addressL2, String addressL3, String country) {
        this.userId = userId;
        this.designation = designation;
        this.organisation = organisation;
        this.email = email;
        this.contactNumber1 = contactNumber1;
        this.contactNumber2 = contactNumber2;
        this.profileSummary = profileSummary;
        this.website = website;
        this.addressL1 = addressL1;
        this.addressL2 = addressL2;
        this.addressL3 = addressL3;
        this.country = country;
    }

    public Short getUserId() {
        return userId;
    }

    public void setUserId(Short userId) {
        this.userId = userId;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOrganisation() {
        return organisation;
    }

    public void setOrganisation(String organisation) {
        this.organisation = organisation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber1() {
        return contactNumber1;
    }

    public void setContactNumber1(String contactNumber1) {
        this.contactNumber1 = contactNumber1;
    }

    public String getContactNumber2() {
        return contactNumber2;
    }

    public void setContactNumber2(String contactNumber2) {
        this.contactNumber2 = contactNumber2;
    }

    public String getProfileSummary() {
        return profileSummary;
    }

    public void setProfileSummary(String profileSummary) {
        this.profileSummary = profileSummary;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddressL1() {
        return addressL1;
    }

    public void setAddressL1(String addressL1) {
        this.addressL1 = addressL1;
    }

    public String getAddressL2() {
        return addressL2;
    }

    public void setAddressL2(String addressL2) {
        this.addressL2 = addressL2;
    }

    public String getAddressL3() {
        return addressL3;
    }

    public void setAddressL3(String addressL3) {
        this.addressL3 = addressL3;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BusinessCard)) {
            return false;
        }
        BusinessCard other = (BusinessCard) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.BusinessCard[ userId=" + userId + " ]";
    }
    
}
