/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "admin_message")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AdminMessage.findAll", query = "SELECT a FROM AdminMessage a"),
    @NamedQuery(name = "AdminMessage.findByMessageId", query = "SELECT a FROM AdminMessage a WHERE a.messageId = :messageId"),
    @NamedQuery(name = "AdminMessage.findByUserId", query = "SELECT a FROM AdminMessage a WHERE a.userId = :userId"),
    @NamedQuery(name = "AdminMessage.findBySubjecct", query = "SELECT a FROM AdminMessage a WHERE a.subjecct = :subjecct"),
    @NamedQuery(name = "AdminMessage.findByMessage", query = "SELECT a FROM AdminMessage a WHERE a.message = :message"),
    @NamedQuery(name = "AdminMessage.findByState", query = "SELECT a FROM AdminMessage a WHERE a.state = :state"),
    @NamedQuery(name = "AdminMessage.findByTimeStamp", query = "SELECT a FROM AdminMessage a WHERE a.timeStamp = :timeStamp")})
public class AdminMessage implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "message_id")
    private Short messageId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "subjecct")
    private String subjecct;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Column(name = "state")
    private int state;
    @Basic(optional = false)
    @NotNull
    @Column(name = "time_stamp")
    @Temporal(TemporalType.TIMESTAMP)
    private Date timeStamp;

    public AdminMessage() {
    }

    public AdminMessage(Short messageId) {
        this.messageId = messageId;
    }

    public AdminMessage(Short messageId, short userId, String subjecct, String message, int state, Date timeStamp) {
        this.messageId = messageId;
        this.userId = userId;
        this.subjecct = subjecct;
        this.message = message;
        this.state = state;
        this.timeStamp = timeStamp;
    }

    public Short getMessageId() {
        return messageId;
    }

    public void setMessageId(Short messageId) {
        this.messageId = messageId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public String getSubjecct() {
        return subjecct;
    }

    public void setSubjecct(String subjecct) {
        this.subjecct = subjecct;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (messageId != null ? messageId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AdminMessage)) {
            return false;
        }
        AdminMessage other = (AdminMessage) object;
        if ((this.messageId == null && other.messageId != null) || (this.messageId != null && !this.messageId.equals(other.messageId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AdminMessage[ messageId=" + messageId + " ]";
    }
    
}
