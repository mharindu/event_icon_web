/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "user_request")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserRequest.findAll", query = "SELECT u FROM UserRequest u"),
    @NamedQuery(name = "UserRequest.findByRequestId", query = "SELECT u FROM UserRequest u WHERE u.requestId = :requestId"),
    @NamedQuery(name = "UserRequest.findByUserId", query = "SELECT u FROM UserRequest u WHERE u.userId = :userId"),
    @NamedQuery(name = "UserRequest.findByEventId", query = "SELECT u FROM UserRequest u WHERE u.eventId = :eventId")})
public class UserRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "request_id")
    private Short requestId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;

    public UserRequest() {
    }

    public UserRequest(Short requestId) {
        this.requestId = requestId;
    }

    public UserRequest(Short requestId, short userId, short eventId) {
        this.requestId = requestId;
        this.userId = userId;
        this.eventId = eventId;
    }

    public Short getRequestId() {
        return requestId;
    }

    public void setRequestId(Short requestId) {
        this.requestId = requestId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requestId != null ? requestId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserRequest)) {
            return false;
        }
        UserRequest other = (UserRequest) object;
        if ((this.requestId == null && other.requestId != null) || (this.requestId != null && !this.requestId.equals(other.requestId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.UserRequest[ requestId=" + requestId + " ]";
    }
    
}
