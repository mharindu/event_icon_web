/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class EventPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "org_id")
    private short orgId;

    public EventPK() {
    }

    public EventPK(short eventId, short orgId) {
        this.eventId = eventId;
        this.orgId = orgId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    public short getOrgId() {
        return orgId;
    }

    public void setOrgId(short orgId) {
        this.orgId = orgId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) orgId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventPK)) {
            return false;
        }
        EventPK other = (EventPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.orgId != other.orgId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.EventPK[ eventId=" + eventId + ", orgId=" + orgId + " ]";
    }
    
}
