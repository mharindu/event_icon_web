/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class ExperiencePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "experience_id")
    private short experienceId;

    public ExperiencePK() {
    }

    public ExperiencePK(short userId, short experienceId) {
        this.userId = userId;
        this.experienceId = experienceId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getExperienceId() {
        return experienceId;
    }

    public void setExperienceId(short experienceId) {
        this.experienceId = experienceId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) experienceId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ExperiencePK)) {
            return false;
        }
        ExperiencePK other = (ExperiencePK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.experienceId != other.experienceId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.ExperiencePK[ userId=" + userId + ", experienceId=" + experienceId + " ]";
    }
    
}
