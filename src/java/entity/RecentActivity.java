/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "recent_activity")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RecentActivity.findAll", query = "SELECT r FROM RecentActivity r"),
    @NamedQuery(name = "RecentActivity.findByUserId", query = "SELECT r FROM RecentActivity r WHERE r.recentActivityPK.userId = :userId"),
    @NamedQuery(name = "RecentActivity.findByActId", query = "SELECT r FROM RecentActivity r WHERE r.recentActivityPK.actId = :actId"),
    @NamedQuery(name = "RecentActivity.findByEventName", query = "SELECT r FROM RecentActivity r WHERE r.eventName = :eventName"),
    @NamedQuery(name = "RecentActivity.findByDescription", query = "SELECT r FROM RecentActivity r WHERE r.description = :description")})
public class RecentActivity implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RecentActivityPK recentActivityPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "event_name")
    private String eventName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;

    public RecentActivity() {
    }

    public RecentActivity(RecentActivityPK recentActivityPK) {
        this.recentActivityPK = recentActivityPK;
    }

    public RecentActivity(RecentActivityPK recentActivityPK, String eventName, String description) {
        this.recentActivityPK = recentActivityPK;
        this.eventName = eventName;
        this.description = description;
    }

    public RecentActivity(short userId, short actId) {
        this.recentActivityPK = new RecentActivityPK(userId, actId);
    }

    public RecentActivityPK getRecentActivityPK() {
        return recentActivityPK;
    }

    public void setRecentActivityPK(RecentActivityPK recentActivityPK) {
        this.recentActivityPK = recentActivityPK;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (recentActivityPK != null ? recentActivityPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RecentActivity)) {
            return false;
        }
        RecentActivity other = (RecentActivity) object;
        if ((this.recentActivityPK == null && other.recentActivityPK != null) || (this.recentActivityPK != null && !this.recentActivityPK.equals(other.recentActivityPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.RecentActivity[ recentActivityPK=" + recentActivityPK + " ]";
    }
    
}
