/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class MaterialPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "material_id")
    private short materialId;

    public MaterialPK() {
    }

    public MaterialPK(short eventId, short materialId) {
        this.eventId = eventId;
        this.materialId = materialId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    public short getMaterialId() {
        return materialId;
    }

    public void setMaterialId(short materialId) {
        this.materialId = materialId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) materialId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaterialPK)) {
            return false;
        }
        MaterialPK other = (MaterialPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.materialId != other.materialId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.MaterialPK[ eventId=" + eventId + ", materialId=" + materialId + " ]";
    }
    
}
