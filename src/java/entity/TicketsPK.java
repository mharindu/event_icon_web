/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class TicketsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ticket_id")
    private short ticketId;

    public TicketsPK() {
    }

    public TicketsPK(short eventId, short ticketId) {
        this.eventId = eventId;
        this.ticketId = ticketId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    public short getTicketId() {
        return ticketId;
    }

    public void setTicketId(short ticketId) {
        this.ticketId = ticketId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) ticketId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TicketsPK)) {
            return false;
        }
        TicketsPK other = (TicketsPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.ticketId != other.ticketId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.TicketsPK[ eventId=" + eventId + ", ticketId=" + ticketId + " ]";
    }
    
}
