/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class EventUserPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "event_id")
    private short eventId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;

    public EventUserPK() {
    }

    public EventUserPK(short eventId, short userId) {
        this.eventId = eventId;
        this.userId = userId;
    }

    public short getEventId() {
        return eventId;
    }

    public void setEventId(short eventId) {
        this.eventId = eventId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) eventId;
        hash += (int) userId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof EventUserPK)) {
            return false;
        }
        EventUserPK other = (EventUserPK) object;
        if (this.eventId != other.eventId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.EventUserPK[ eventId=" + eventId + ", userId=" + userId + " ]";
    }
    
}
