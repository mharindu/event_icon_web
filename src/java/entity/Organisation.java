/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "organisation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Organisation.findAll", query = "SELECT o FROM Organisation o"),
    @NamedQuery(name = "Organisation.findByUserId", query = "SELECT o FROM Organisation o WHERE o.organisationPK.userId = :userId"),
    @NamedQuery(name = "Organisation.findByOrganizationId", query = "SELECT o FROM Organisation o WHERE o.organisationPK.organizationId = :organizationId"),
    @NamedQuery(name = "Organisation.findByTitle", query = "SELECT o FROM Organisation o WHERE o.title = :title"),
    @NamedQuery(name = "Organisation.findByDescription", query = "SELECT o FROM Organisation o WHERE o.description = :description"),
    @NamedQuery(name = "Organisation.findByStartDate", query = "SELECT o FROM Organisation o WHERE o.startDate = :startDate"),
    @NamedQuery(name = "Organisation.findByEndDate", query = "SELECT o FROM Organisation o WHERE o.endDate = :endDate")})
public class Organisation implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected OrganisationPK organisationPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_date")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_date")
    @Temporal(TemporalType.DATE)
    private Date endDate;

    public Organisation() {
    }

    public Organisation(OrganisationPK organisationPK) {
        this.organisationPK = organisationPK;
    }

    public Organisation(OrganisationPK organisationPK, String title, String description, Date startDate, Date endDate) {
        this.organisationPK = organisationPK;
        this.title = title;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Organisation(short userId, short organizationId) {
        this.organisationPK = new OrganisationPK(userId, organizationId);
    }

    public OrganisationPK getOrganisationPK() {
        return organisationPK;
    }

    public void setOrganisationPK(OrganisationPK organisationPK) {
        this.organisationPK = organisationPK;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organisationPK != null ? organisationPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organisation)) {
            return false;
        }
        Organisation other = (Organisation) object;
        if ((this.organisationPK == null && other.organisationPK != null) || (this.organisationPK != null && !this.organisationPK.equals(other.organisationPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Organisation[ organisationPK=" + organisationPK + " ]";
    }
    
}
