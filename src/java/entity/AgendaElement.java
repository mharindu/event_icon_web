/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "agenda_element")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AgendaElement.findAll", query = "SELECT a FROM AgendaElement a"),
    @NamedQuery(name = "AgendaElement.findByEventId", query = "SELECT a FROM AgendaElement a WHERE a.agendaElementPK.eventId = :eventId"),
    @NamedQuery(name = "AgendaElement.findByElementId", query = "SELECT a FROM AgendaElement a WHERE a.agendaElementPK.elementId = :elementId"),
    @NamedQuery(name = "AgendaElement.findByTitle", query = "SELECT a FROM AgendaElement a WHERE a.title = :title"),
    @NamedQuery(name = "AgendaElement.findByPresenter", query = "SELECT a FROM AgendaElement a WHERE a.presenter = :presenter"),
    @NamedQuery(name = "AgendaElement.findByDescription", query = "SELECT a FROM AgendaElement a WHERE a.description = :description"),
    @NamedQuery(name = "AgendaElement.findByStartTime", query = "SELECT a FROM AgendaElement a WHERE a.startTime = :startTime"),
    @NamedQuery(name = "AgendaElement.findByEndTime", query = "SELECT a FROM AgendaElement a WHERE a.endTime = :endTime")})
public class AgendaElement implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected AgendaElementPK agendaElementPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "presenter")
    private String presenter;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "start_time")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "end_time")
    @Temporal(TemporalType.TIME)
    private Date endTime;

    public AgendaElement() {
    }

    public AgendaElement(AgendaElementPK agendaElementPK) {
        this.agendaElementPK = agendaElementPK;
    }

    public AgendaElement(AgendaElementPK agendaElementPK, String title, String presenter, String description, Date startTime, Date endTime) {
        this.agendaElementPK = agendaElementPK;
        this.title = title;
        this.presenter = presenter;
        this.description = description;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public AgendaElement(short eventId, short elementId) {
        this.agendaElementPK = new AgendaElementPK(eventId, elementId);
    }

    public AgendaElementPK getAgendaElementPK() {
        return agendaElementPK;
    }

    public void setAgendaElementPK(AgendaElementPK agendaElementPK) {
        this.agendaElementPK = agendaElementPK;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPresenter() {
        return presenter;
    }

    public void setPresenter(String presenter) {
        this.presenter = presenter;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (agendaElementPK != null ? agendaElementPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AgendaElement)) {
            return false;
        }
        AgendaElement other = (AgendaElement) object;
        if ((this.agendaElementPK == null && other.agendaElementPK != null) || (this.agendaElementPK != null && !this.agendaElementPK.equals(other.agendaElementPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.AgendaElement[ agendaElementPK=" + agendaElementPK + " ]";
    }
    
}
