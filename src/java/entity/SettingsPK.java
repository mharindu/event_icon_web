/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class SettingsPK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "setting_id")
    private short settingId;

    public SettingsPK() {
    }

    public SettingsPK(short userId, short settingId) {
        this.userId = userId;
        this.settingId = settingId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getSettingId() {
        return settingId;
    }

    public void setSettingId(short settingId) {
        this.settingId = settingId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) settingId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SettingsPK)) {
            return false;
        }
        SettingsPK other = (SettingsPK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.settingId != other.settingId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SettingsPK[ userId=" + userId + ", settingId=" + settingId + " ]";
    }
    
}
