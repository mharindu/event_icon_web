/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Harindu
 */
@Embeddable
public class PublicProfilePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "user_id")
    private short userId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "profile_id")
    private short profileId;

    public PublicProfilePK() {
    }

    public PublicProfilePK(short userId, short profileId) {
        this.userId = userId;
        this.profileId = profileId;
    }

    public short getUserId() {
        return userId;
    }

    public void setUserId(short userId) {
        this.userId = userId;
    }

    public short getProfileId() {
        return profileId;
    }

    public void setProfileId(short profileId) {
        this.profileId = profileId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) userId;
        hash += (int) profileId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PublicProfilePK)) {
            return false;
        }
        PublicProfilePK other = (PublicProfilePK) object;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.profileId != other.profileId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.PublicProfilePK[ userId=" + userId + ", profileId=" + profileId + " ]";
    }
    
}
