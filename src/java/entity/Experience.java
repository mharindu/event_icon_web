/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Harindu
 */
@Entity
@Table(name = "experience")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Experience.findAll", query = "SELECT e FROM Experience e"),
    @NamedQuery(name = "Experience.findByUserId", query = "SELECT e FROM Experience e WHERE e.experiencePK.userId = :userId"),
    @NamedQuery(name = "Experience.findByExperienceId", query = "SELECT e FROM Experience e WHERE e.experiencePK.experienceId = :experienceId"),
    @NamedQuery(name = "Experience.findByTitle", query = "SELECT e FROM Experience e WHERE e.title = :title"),
    @NamedQuery(name = "Experience.findByDescription", query = "SELECT e FROM Experience e WHERE e.description = :description"),
    @NamedQuery(name = "Experience.findByYear", query = "SELECT e FROM Experience e WHERE e.year = :year")})
public class Experience implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ExperiencePK experiencePK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "title")
    private String title;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Column(name = "year")
    @Temporal(TemporalType.DATE)
    private Date year;

    public Experience() {
    }

    public Experience(ExperiencePK experiencePK) {
        this.experiencePK = experiencePK;
    }

    public Experience(ExperiencePK experiencePK, String title, String description, Date year) {
        this.experiencePK = experiencePK;
        this.title = title;
        this.description = description;
        this.year = year;
    }

    public Experience(short userId, short experienceId) {
        this.experiencePK = new ExperiencePK(userId, experienceId);
    }

    public ExperiencePK getExperiencePK() {
        return experiencePK;
    }

    public void setExperiencePK(ExperiencePK experiencePK) {
        this.experiencePK = experiencePK;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getYear() {
        return year;
    }

    public void setYear(Date year) {
        this.year = year;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (experiencePK != null ? experiencePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Experience)) {
            return false;
        }
        Experience other = (Experience) object;
        if ((this.experiencePK == null && other.experiencePK != null) || (this.experiencePK != null && !this.experiencePK.equals(other.experiencePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Experience[ experiencePK=" + experiencePK + " ]";
    }
    
}
